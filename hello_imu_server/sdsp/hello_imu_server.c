/****************************************************************************
 *
 * Copyright (c) 2015 Mark Charlebois. All rights reserved.
 *
 * Copyright (c) 2020 ModalAI, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name ATLFlight nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <pthread.h>
#include <unistd.h>
#include <string.h>

#include "utils.h"
#include "hello_imu_server.h"

#include "imu_server_api.h"

static pthread_t      new_thread;
static pthread_attr_t new_thread_attr;
static int            keep_running = 1;
static int            thread_running = 0;

static int64_t get_time_monotonic_ns()
{
    struct timespec ts;
    if(clock_gettime(CLOCK_MONOTONIC, &ts)) return -1;
    int64_t t = (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
    return t;
}

static int64_t get_time_realtime_ns()
{
    struct timespec ts;
    if(clock_gettime(CLOCK_REALTIME, &ts)) return -1;
    int64_t t = (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
    return t;
}

static void* new_thread_func(void *arg) {
    LOG_MSG("Hello from inside the new thread");

    //populate the IMU device info
    sensor_imu_type imu_type   = SENSOR_IMU_TYPE_FLIGHT_PRIMARY;
    char * imu_dev_desc        = "Primary IMU";
    int imu_sample_buffer_size = 100;

    //register the IMU device with IMU server
    imu_device * imu_dev = sensor_imuserver_cb_register_imu_device(
                                imu_type,
                                imu_dev_desc,strlen(imu_dev_desc)+1,
                                imu_sample_buffer_size);

    if (imu_dev == NULL){
        LOG_MSG("Could not get register imu device with imu server");
        return NULL;
    }

    //populate the IMU device properties
    sensor_mpu_driver_settings settings;
    memset(&settings, 0, sizeof(sensor_mpu_driver_settings));

    settings.sample_rate_in_hz         = 500;
    settings.compass_sample_rate_in_hz = 0;
    settings.accel_lpf_in_hz           = 91;
    settings.gyro_lpf_in_hz            = 91;

    //tell the IMU server that IMU device is ready to push data
    int16_t server_init_ret = sensor_imuserver_cb_driver_initialized(imu_dev, &settings);
    if (server_init_ret != 0){
        LOG_MSG("Could not get initialize the imu server");
        return NULL;
    }

    uint32_t seq_num = 0;

    while(keep_running)
    {
        //generate fake IMU data
        sensor_imu imu_data;
        memset(&imu_data, 0, sizeof(imu_data));

        //WARNING: IMU server incorrectly overwrites raw_timestamp_in_us with timestamp_in_us
        //before applying clock sync offset to timestamp_in_us
        //to fix this, assign the same timestamp to both timestamp_in_us and raw_timestamp_in_us
        imu_data.timestamp_in_us        = get_time_monotonic_ns()/1000;  //will be updated by IMU server with adjusted timestamp
        imu_data.raw_timestamp_in_us    = get_time_monotonic_ns()/1000;
        imu_data.sequence_number        = 0;      //will be populated by IMU server (on DSP side)
        imu_data.linear_acceleration[0] = 0.01;   //units: Gs
        imu_data.linear_acceleration[1] = 0.02;
        imu_data.linear_acceleration[2] = 1.0 + (seq_num % 100)*0.001; //add some time-varying component
        imu_data.angular_velocity[0]    = +0.01;  //units: rad/s
        imu_data.angular_velocity[1]    = -0.02;
        imu_data.angular_velocity[2]    = +0.03;
        imu_data.temperature            = 37.0;   //units: deg C

        int ret = sensor_imuserver_cb_imu_data_received(imu_dev, &imu_data);

        if (ret){
            LOG_MSG("Could not push imu data to imu server.. Exiting");
            break;
        }

        seq_num++;

        usleep(2000);  //note that loop will run slightly longer than sleep time
    }

    sensor_imuserver_cb_driver_closed(imu_dev);  //unregister the imu device


    usleep(100000);
    LOG_MSG("Exiting the new thread");

    return NULL;
}

uint32_t hello_imu_server_stop_imu()
{
    keep_running = 0;
    if (thread_running == 0){
        LOG_MSG("thread is not running");
        return 0;
    }

    //wait until thread is finished
    if (pthread_join(new_thread, NULL) != 0)
    {
        LOG_MSG("pthread_join returned error");
        return -1;
    }
    thread_running = 0;

    LOG_MSG("New thread finished");

    return 0;
}


uint32_t hello_imu_server_start_imu()
{
    LOG_MSG("Starting IMU");

    keep_running = 1;

    if (pthread_attr_init(&new_thread_attr) != 0)
    {
      LOG_MSG("pthread_attr_init returned error");
      return -1;
    }

    //set stack size (required to start a new thread)
    size_t stack_size = 4 * 1024;
    if (pthread_attr_setstacksize(&new_thread_attr, stack_size) != 0)
    {
        LOG_MSG("pthread_attr_setstacksize returned error");
        return -1;
    }

    //create a new thread
    if (pthread_create(&new_thread, &new_thread_attr, new_thread_func, NULL) != 0)
    {
    	LOG_MSG("pthread_create returned error");
    	return -1;
    }

    thread_running = 1;
    LOG_MSG("New thread creation was successful");

    return 0;
}
