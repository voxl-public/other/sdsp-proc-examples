/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

 #pragma once
 #include <stdint.h>

 #ifdef __cplusplus
 extern "C" {
 #endif

 #define SENSOR_DATATYPES_3x1_VECTOR_SIZE 3
 #define SENSOR_DATATYPES_3x3_MAT_SIZE    9
 #define SENSOR_IMU_MAX_DESCRIPTION_LEN   256

 /**
  * IMU type which should be set by the IMU driver to tell the clients what the IMU is used for.
  **/
 typedef enum
 {
   SENSOR_IMU_TYPE_BEGIN = 0,
   SENSOR_IMU_TYPE_FLIGHT_PRIMARY,
   SENSOR_IMU_TYPE_FLIGHT_SECONDARY,
   SENSOR_IMU_TYPE_CAMERA_NAV,
   SENSOR_IMU_TYPE_CAMERA_VIDEO,
   SENSOR_IMU_TYPE_CAMERA_STEREO,
   SENSOR_IMU_TYPE_OTHER,
   SENSOR_IMU_TYPE_COUNT
 }sensor_imu_type;

 typedef enum
 {
   X_AXIS = 0,
   Y_AXIS,
   Z_AXIS,
   IMU_MAX_DIMENSIONS
 } sensor_imu_axis;


 /**
  * Enum to tell the IMU server which clock source to use for synchronizing
  * dsp time to apps time. To be used by API implementation on DSP.
  **/
 typedef enum {
     SENSOR_CLOCK_SYNC_TYPE_MONOTONIC = 0,
     SENSOR_CLOCK_SYNC_TYPE_REALTIME
 } sensor_clock_sync_type;


 typedef struct {
   int16_t         imu_id;
   sensor_imu_type imu_type;
   int32_t         imu_max_buffer_size;
   char            imu_description[SENSOR_IMU_MAX_DESCRIPTION_LEN];
 } sensor_imu_id;


 typedef struct {
   /**
    * Timestamp in microseconds synced to the apps clock source
    * timestamp_in_us = raw_timestamp_in_us + ticks_imu_clock_behind_apps_clock
    * this field is populated by the IMU server
    */
   uint64_t timestamp_in_us;

   /**
    * Timestamp in microseconds when IMU sample was taken (using DSP clock)
    */
   uint64_t raw_timestamp_in_us;

   /**
    * Incremental counter of the IMU data. This field is updated by the IMU server.
    */
   uint32_t sequence_number;

   /**
    * Accelemeter data, raw or calibrated depending on the API used to get the data.
    * The units are Gs
    */
   float    linear_acceleration[SENSOR_DATATYPES_3x1_VECTOR_SIZE];

   /**
    * Gyro data, raw or calibrated based on the API used to get the data.
    * The units are rad/sec
    */
   float    angular_velocity[SENSOR_DATATYPES_3x1_VECTOR_SIZE];

   /**
    * IMU Temperature reading in degrees C.
    */
   float    temperature;
 } __attribute__((packed)) sensor_imu;

 /**
  * Imu sensor settings that are used to pass to the clients
  **/
 typedef struct {
   /**
    * if the driver is not initialized, this is set 0.
    * if the driver is initialized, this is set to 1
    **/
   int16_t is_initialized;

   /**
    * Data sample rate for both accel and gyro.
    * a value if -1 is invalid
    **/
   int16_t sample_rate_in_hz;

   /**
    * a value of -1 for any of the setting below is an invalid value.
    * The value of -1 is returned if the is_initialized==0
    **/
   int16_t compass_sample_rate_in_hz;
   int16_t accel_lpf_in_hz;
   int16_t gyro_lpf_in_hz;
 } __attribute__((packed)) sensor_mpu_driver_settings;


 /**
  * Rotation Matrix that represents attitude estimate as determined by the flight stack.
  * The included IMU data should be bias-compensated, if available.
  * Note:
  *     The field: timestamp_in_us - represents the timestamp adjusted to the apps processor.
  *     The field: raw_timestamp_in_us --> Represents the raw adsp timestamp of when the sample was taken.
  */
 typedef struct {
   /**
    * Timestamp in microseconds synced to the apps clock source
    * timestamp_in_us = raw_timestamp_in_us + ticks_imu_clock_behind_apps_clock
    * this field is populated by the IMU server
    */
   uint64_t timestamp_in_us;

   /**
    * Timestamp in microseconds when IMU sample was taken (using DSP clock)
    */
   uint64_t raw_timestamp_in_us;

   /**
    * Incremental counter of the IMU data. This field is updated by the IMU server.
    */
   uint32_t sequence_number;

   /**
    * Accelemeter data, raw or calibrated depending on the API used to get the data.
    * The units are Gs
    */
   float    linear_acceleration[SENSOR_DATATYPES_3x1_VECTOR_SIZE];

   /**
    * Gyro data, raw or calibrated based on the API used to get the data.
    * The units are rad/sec
    */
   float    angular_velocity[SENSOR_DATATYPES_3x1_VECTOR_SIZE];

   /**
    * Attitude estimate rotation matrix
    */
   float rotation_matrix[ SENSOR_DATATYPES_3x3_MAT_SIZE ];
 } __attribute__((packed)) sensor_attitude;



 typedef struct imu_device imu_device;
 /**
  * Structure to hold the bias estimations as computed by the flight stack.
  */
 typedef struct {
   float linear_accel_bias_offset[IMU_MAX_DIMENSIONS];
   float angular_velocity_bias_offset[IMU_MAX_DIMENSIONS];
 } __attribute__((packed)) sensor_imu_bias_offsets;



 /**
  * Register the IMU device with the imu server.
  * Use the returned handle to call the other callback methods.
  * @param imu_type
  *   Specify the IMU function.
  * @param description
  *   A char buffer to describe the IMU device.
  * @param size_in_bytes
  *    The size of the description buffer.
  * @param buffer_size
  *    The size of the Server Buffer in sample count.
  * @return
  *   0     ---> failure
  *   other ---> success;
  */
 imu_device* sensor_imuserver_cb_register_imu_device
 (
   sensor_imu_type imu_type,
   char*           description,
   int32_t         size_in_bytes,
   int32_t         buffer_size
 );


 /**
  * Notify the IMU server that IMU driver is ready to stream data
  * @param dev_id
  *   device handle returned by register_imu_device
  * @param driver_settings
  *   Provide the IMU configuration / settings
  */
 int16_t sensor_imuserver_cb_driver_initialized( imu_device* dev_id, sensor_mpu_driver_settings* driver_settings );

 /**
  * Push a single IMU data packet to the IMU server
  * @param dev_id
  *   device handle returned by register_imu_device
  * @param data
  *   IMU data
  * @return
  *   0         = succesful
  *   otherwise = error/failure.
  */
 int16_t sensor_imuserver_cb_imu_data_received( imu_device* dev_id, sensor_imu* data );



 /**
  * Notify the IMU server to close the IMU device
 * @param dev_id
 *   device handle returned by register_imu_device
  * @return
  *   0         = success
  *   otherwise = error/failure.
  **/
 int16_t sensor_imuserver_cb_driver_closed( imu_device* dev_id );



 #ifdef __cplusplus
 }
 #endif
