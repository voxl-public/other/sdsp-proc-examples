# SLPI "Hello World" Example
This project **requires the Hexagon SDK Cross-Compiler Environment** available at voxl-docker (https://gitlab.com/voxl-public/voxl-docker).

1. Build sDSP/SLPI libraries and APP processor binary
```bash
$ cd helloworld
$ voxl-docker -i voxl-hexagon
$ ./build.sh
```

2. Confirm that target is found with adb
```bash
> adb devices
List of devices attached 
1a2b3c4d	device
```

3. Push binaries and libraries to target
```bash
$ ./deploy_to_target
```

4. To view output from the DSP run Hexagon debug tool mini-dm (within voxl-hexagon docker image)
```bash
$ ${HEXAGON_SDK_ROOT}/tools/debug/mini-dm/Linux_Debug/mini-dm
```

5. In a new shell run example on target
```bash
$ adb shell
$ cd home/root
$ ./helloworld
```

You should see the following output in the terminal running mini-dm:
```
Running mini-dm version: 3.1
<various initialization outputs>
DMSS is connected. Running mini-dm...
----------------Mini-dm is ready to log----------------
<warnings you can ignore>
[08500/00]  --:--.---  Hello World  0036  helloworld.c
```
