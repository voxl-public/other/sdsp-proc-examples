/****************************************************************************
 *
 * Copyright (c) 2015 Mark Charlebois. All rights reserved.
 *
 * Copyright (c) 2020 ModalAI, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name ATLFlight nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <pthread.h>
#include <unistd.h>

#include "utils.h"
#include "hello_threads.h"


void* new_thread_func(void *arg) {
    LOG_MSG("Hello from inside the new thread");
    LOG_MSG("Sleeping inside the thread");
	usleep(100000);
    LOG_MSG("Exiting the new thread");

    return NULL;
}

uint32_t hello_threads_say_hello()
{
    LOG_MSG("Hello Threads!");

    pthread_t new_thread;
    pthread_attr_t new_thread_attr;

    if (pthread_attr_init(&new_thread_attr) != 0){
        LOG_MSG("pthread_attr_init returned error");
        return -1;
    }

    //set stack size
    size_t stack_size = 4 * 1024;
    LOG_MSG("Setting stack size to %d",stack_size);
    if (pthread_attr_setstacksize(&new_thread_attr, stack_size) != 0){
        LOG_MSG("pthread_attr_setstacksize returned error");
        return -1;
    }

    //create a new thread
    if (pthread_create(&new_thread, &new_thread_attr, new_thread_func, NULL) != 0){
        LOG_MSG("pthread_create returned error");
        return -1;
    }
    else{
        LOG_MSG("New thread creation was successful");
    }

    //wait until new thread is finished
    if (pthread_join(new_thread, NULL) != 0){
        LOG_MSG("pthread_join returned error");
        return -1;
    }

    LOG_MSG("New thread finished");

    LOG_MSG("Bye bye :)");

    return 0;
}
