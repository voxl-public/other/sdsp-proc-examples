/****************************************************************************
 * 
 * Copyright (c) 2015 James Wilson. All rights reserved.
 * 
 * Copyright (c) 2019 ModalAI, Inc. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name ATLFlight nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 ****************************************************************************/

#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>

#include "rpcmem.h"
#include "slpi_uart.h"
#include "c_library_v2/common/mavlink.h"

/* Use the following macro for debug output on the Application Processor. */
#define LOG_INFO(...)	if (debug_flag) { printf(__VA_ARGS__); printf("\n"); }
#define LOG_ERR(...)	if (debug_flag) { printf(__VA_ARGS__); printf("\n"); }
#define LOG_DEBUG(...)	if (debug_flag) { printf(__VA_ARGS__); printf("\n"); }

#define FASTRPC_HEAP_ID 22 // FIXME - no idea how these are allocated
#define FASTRPC_FLAGS 0

#define QGC_PORT 14550
#define MAX_DATA_BUFFER_LEN 1024

int debug_flag = 0;

static int gc_sockfd;
struct sockaddr_in gc_servaddr;

uint8_t message_buffer[MAX_DATA_BUFFER_LEN];

static pthread_mutex_t slpi_mutex = PTHREAD_MUTEX_INITIALIZER;

void *slpi_uart_thread(void *vargp)
{
    int status = 0;

    LOG_INFO("Read thread starting");
    uint8_t *rpc_data_buffer = (uint8_t*)rpcmem_alloc(FASTRPC_HEAP_ID, FASTRPC_FLAGS, MAX_DATA_BUFFER_LEN);
    if (rpc_data_buffer == NULL)
    {
        LOG_ERR("Read thread couldn't allocate buffer, exiting");
        return NULL;
    }

    uint8_t *data_buffer = &rpc_data_buffer[0];
    uint32_t amount_read = 1;
    mavlink_message_t msg;
    mavlink_status_t  msg_status;

    while (amount_read > 0)
    {
        pthread_mutex_lock( &slpi_mutex );
        status = slpi_uart_read(data_buffer, MAX_DATA_BUFFER_LEN, &amount_read);
        pthread_mutex_unlock( &slpi_mutex );

        if (status != 0) {
            // Disabling uart read debug print as this floods output
            // LOG_ERR("Failed to read from SLPI UART. Status: %d", status);
            usleep(1000);
        }
        else
        {
            time_t current_time = time(NULL);

            LOG_INFO("Successfully read %d bytes at time %ld", amount_read, (long)current_time);

            for (unsigned int i = 0; i < amount_read; i++)
            {
                if (mavlink_parse_char(0, data_buffer[i], &msg, &msg_status))
                {
                    uint16_t msg_len = mavlink_msg_to_send_buffer(message_buffer, &msg);
                    LOG_INFO("Received message with ID %d, sequence: %d. msg len %d",
                             msg.msgid, msg.seq, msg_len);

                    // TODO: Watch out for race conditions on gc_servaddr. Add a mutex.
                    sendto(gc_sockfd, message_buffer, msg_len,
                           MSG_CONFIRM, (const struct sockaddr*)&gc_servaddr,
                           sizeof(gc_servaddr));
                }
            }
            usleep(50000);
        }
    }

    // Release allocated RPC memory buffer
    if (rpc_data_buffer != NULL) {
        rpcmem_free(rpc_data_buffer);
        rpc_data_buffer = 0;
    }

    LOG_INFO("Read thread ending");

    return NULL;
}

void usage()
{
    printf("Error: Provide ground control server IP address on command line\n");
    printf("       Example: telemetry_proxy 10.8.0.10\n");
}

int main(int argc, char** argv)
{
    int status = 0;

    // Check that ground control IP address has bee provided
    if (argc < 2)
    {
        usage();
        return -1;
    }

    // If more than 1 argument has been provided enable debug output
    if (argc > 2)
    {
        debug_flag = 1;
    }

    // Call once at beginning to initialize RPC
    rpcmem_init();

    // Creating socket file descriptor for ground control connection
    if ( (gc_sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        LOG_ERR("GC socket creation failed");
        return -1;
    }

    struct sockaddr_in gc_recvaddr;
    memset(&gc_servaddr, 0, sizeof(gc_servaddr));
    memset(&gc_recvaddr, 0, sizeof(gc_recvaddr));

    // QGroundControl server information.
    // TODO: We should broadcast until it responds and then switch to the
    // actual IP address. For now we just accept the address on the command line
    // because broadcast is not supported on VPN tunnel.
    gc_servaddr.sin_family = AF_INET; // IPv4
    //gc_servaddr.sin_addr.s_addr = htonl(INADDR_BROADCAST); // Broadcast
    gc_servaddr.sin_addr.s_addr = inet_addr(argv[1]); // input QGC IP
    gc_servaddr.sin_port = htons(QGC_PORT);

    socklen_t len = sizeof(gc_recvaddr);
    ssize_t n;

    char buffer[MAX_DATA_BUFFER_LEN];

    // Allocate RPC memory buffer to write to SLPI UART
    uint8_t *rpc_data_buffer = (uint8_t*)rpcmem_alloc(FASTRPC_HEAP_ID, FASTRPC_FLAGS, MAX_DATA_BUFFER_LEN);
    if (rpc_data_buffer == NULL) {
        LOG_ERR("%s rpcmem_alloc failed! for data buffer", __FILE__);
        rpcmem_free(rpc_data_buffer);
        return 1;
    }

    // Create thread to read received messages from SLPI UART and transmit to ground control
    pthread_t slpi_uart_thread_id;
    pthread_create(&slpi_uart_thread_id, NULL, slpi_uart_thread, NULL);

    uint8_t *data_buffer = &rpc_data_buffer[0];

    int running = 1;
    while (running)
    {
        // Receive UTP message from ground control
        n = recvfrom(gc_sockfd, (char*)buffer, MAX_DATA_BUFFER_LEN,
                     MSG_WAITALL, (struct sockaddr*)&gc_recvaddr,
                     &len);
                     
        LOG_INFO("Got %d bytes from ground control", n);
        for (int i = 0; i < n; i++)
        {
            data_buffer[i] = (uint8_t)buffer[i];
        }

        // Write received bytes to slpi
        pthread_mutex_lock( &slpi_mutex );
        status = slpi_uart_write(data_buffer, n);
        pthread_mutex_unlock( &slpi_mutex );

        if (status != 0)
        {
            LOG_INFO("Failed to write to SLPI UART. %d", status);
            running = 0;
        }

        usleep(100);
    }

    // Wait for the other thread to return
    pthread_join(slpi_uart_thread_id, NULL);

    // Call once at the end to clean up RPC
    rpcmem_deinit();

    return status;
}
