/****************************************************************************
 * 
 * Copyright (c) 2015 James Wilson. All rights reserved.
 * 
 * Copyright (c) 2018 Modal AI. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name ATLFlight nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <HAP_farf.h>

/* Use the following macro for debug output on the aDSP. */
#define LOG_INFO(...) FARF(ALWAYS, __VA_ARGS__);
#define LOG_ERR(...) FARF(ALWAYS, __VA_ARGS__);
#define LOG_DEBUG(...) FARF(MEDIUM, __VA_ARGS__);

/* Corresponds to VOXL J# connector. */
#define UART_PORT "/dev/tty-5"

static int global_fd = -1;
static int uart_initialized = 0;

int open_serial_port()
{
    int fd = 0;

    if (!uart_initialized)
    {
        fd = open(UART_PORT, O_RDWR);

        struct termios options;
        tcgetattr(fd, &options);
        cfsetispeed(&options, B57600);
        cfsetospeed(&options, B57600);

        options.c_oflag &= ~(OPOST);

        tcsetattr(fd, TCSANOW, &options);

        global_fd = fd;
        uart_initialized = 1;
    }
    else
    {
        fd = global_fd;
    }

    return fd;
}

long slpi_uart_write(uint8* data_buffer, int data_buffer_len)
{
    long status = 0;

    int uart_dev_fd = open_serial_port();
    int num_bytes_written = write(uart_dev_fd, data_buffer, data_buffer_len);
    if (num_bytes_written != data_buffer_len)
    {
        LOG_ERR("%s: Write failure. expected %d, actual %d", __func__, data_buffer_len, num_bytes_written);
        status = -1;
    }
    else
    {
        LOG_INFO("%s: Write successful", __func__);
    }

    return status;
}

long slpi_uart_read(uint8* data_buffer, int data_buffer_len, int* data_buffer_needed)
{
    long status = 0;

    int num_bytes_read = 0;
    int uart_dev_fd = open_serial_port();

    *data_buffer_needed = 0;

    num_bytes_read = read(uart_dev_fd, data_buffer, data_buffer_len);
    if (num_bytes_read < 0)
    {
        LOG_ERR("%s: read error: %s", __func__, strerror(errno));
        status = -1;
    }
    else if (num_bytes_read == 0)
    {
        LOG_ERR("%s: read returned zero", __func__);
        status = -1;
    }
    else
    {
        LOG_INFO("%s: read successful. %d bytes (0x%x)", __func__, num_bytes_read, num_bytes_read);
        *data_buffer_needed = num_bytes_read;
    }

    return status;
}
